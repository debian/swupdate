#!/usr/bin/make -f
include /usr/share/dpkg/architecture.mk

ifneq ($(DEB_BUILD_GNU_TYPE),$(DEB_HOST_GNU_TYPE))
export CROSS_COMPILE ?= $(DEB_HOST_GNU_TYPE)-
endif

export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export DEB_CFLAGS_MAINT_APPEND = -I/usr/include/wolfssl -I/usr/include/p11-kit-1
export LIBDIR=usr/lib/${DEB_HOST_MULTIARCH}
export HAVE_LUA=n
ifeq (,$(filter nolua,$(DEB_BUILD_PROFILES)))
export LUA_VERSION=5.4
export LUA_MODNAME=lua_swupdate
export PKG_NAME=swupdate
export HAVE_LUA=y
endif

override_dh_auto_configure-arch:
	cp debian/configs/defconfig  configs/debian_defconfig
ifeq (linux,$(DEB_HOST_ARCH_OS))
	echo CONFIG_BTRFS_FILESYSTEM=y     >> configs/debian_defconfig
	echo CONFIG_UBOOT=y                >> configs/debian_defconfig
	echo CONFIG_UCFWHANDLER=y          >> configs/debian_defconfig
	echo CONFIG_CFI=y                  >> configs/debian_defconfig
	echo CONFIG_CFIHAMMING1=y          >> configs/debian_defconfig
	echo CONFIG_UBIVOL=y               >> configs/debian_defconfig
	echo CONFIG_SSBLSWITCH=y           >> configs/debian_defconfig
else
	echo "# CONFIG_MTD is not set"     >> configs/debian_defconfig
endif
ifneq (,$(findstring $(DEB_HOST_ARCH),amd64 i386 arm64 armhf loong64 riscv64 ia64))
	echo CONFIG_BOOTLOADER_EBG=y                    >> configs/debian_defconfig
endif
ifeq (,$(filter pkg.swupdate.nohwcompat,$(DEB_BUILD_PROFILES)))
	echo CONFIG_HW_COMPATIBILITY=y                   >> configs/debian_defconfig
endif
ifeq (,$(filter pkg.swupdate.nosigning,$(DEB_BUILD_PROFILES)))
	echo CONFIG_SIGNED_IMAGES=y >> configs/debian_defconfig
endif
ifeq (,$(filter pkg.swupdate.noswuforwarder,$(DEB_BUILD_PROFILES)))
	echo CONFIG_SWUFORWARDER_HANDLER=y       >> configs/debian_defconfig
endif
ifneq (,$(filter pkg.swupdate.p11,$(DEB_BUILD_PROFILES)))
	echo CONFIG_PKCS11=y >> configs/debian_defconfig
endif
ifneq (,$(LUA_VERSION))
	echo CONFIG_LUAPKG=\"lua$(LUA_VERSION)\" >> configs/debian_defconfig
	echo CONFIG_LUASCRIPTHANDLER=y           >> configs/debian_defconfig
	echo CONFIG_SURICATTA_LUA=y              >> configs/debian_defconfig
	echo CONFIG_SURICATTA_WFX=y              >> configs/debian_defconfig
	echo CONFIG_EMBEDDED_SURICATTA_LUA=y     >> configs/debian_defconfig
endif
	echo CONFIG_DELTA=y                                >> configs/debian_defconfig
	echo CONFIG_EXTRA_CFLAGS=\"$(CFLAGS) $(CPPFLAGS)\" >> configs/debian_defconfig
	echo CONFIG_EXTRA_LDFLAGS=\"$(LDFLAGS)\"           >> configs/debian_defconfig
	echo CONFIG_EXTRA_LDLIBS=\"$(LDLIBS)\"             >> configs/debian_defconfig
	$(MAKE) debian_defconfig

override_dh_auto_build-indep:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS) $(DEB_BUILD_PROFILES)))
	$(MAKE) latexpdf
endif
	# Build web-app without gulp
	cd web-app && pkgjs-install-minimal
	sassc -Iweb-app web-app/scss/bootstrap.scss > web-app/css/bootstrap.css
	cp web-app/index.html index.html
	# replace rule from gulp's minify-html
	sed -i 's|node_modules/bootstrap/dist/css|css|' index.html
	# replace upstream bootstrap location with Debian's
	sed -i 's|node_modules/bootstrap/dist/js|node_modules/bootstrap/js|' index.html

override_dh_auto_build-arch:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS) $(DEB_BUILD_PROFILES)))
	$(MAKE) man
	mv doc/build/man/client.1 doc/build/man/swupdate-client.1
	mv doc/build/man/ipc.1 doc/build/man/swupdate-ipc.1
	mv doc/build/man/progress.1 doc/build/man/swupdate-progress.1
endif
	dh_auto_build -- V=1

override_dh_auto_test-arch:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS) $(DEB_BUILD_PROFILES)))
ifeq (,$(findstring mips,$(DEB_HOST_ARCH)))
	dh_auto_test -- V=1
endif
endif

override_dh_auto_install-arch:
	dh_auto_install -- V=1

override_dh_auto_test-indep:
override_dh_auto_install-indep:

# Can be dropped in compat level 14
execute_after_dh_installinit:
	dh_installsysusers

override_dh_installsystemd:
	dh_installsystemd --no-start
	dh_installsystemd --name=swupdate-progress
	dh_installsystemd --no-start --name=swupdate-usb@

ifeq (linux,$(DEB_HOST_ARCH_OS))
override_dh_gencontrol-arch:
	dh_gencontrol -- -VBuilt-Using="$(shell dpkg-query -f '$${source:Package} (= $${source:Version}), ' -W libmtd-dev libubi-dev)"
endif

override_dh_gencontrol-indep:
	# Bootstrap's scss file is derived from
	dh_gencontrol -- -VB-U-www="$(shell dpkg-query -f '$${source:Package} (= $${source:Version}), ' -W node-bootstrap)"

override_dh_dwz:
	dh_dwz --no-dwz-multifile

override_dh_missing:
	dh_missing --fail-missing

%:
	dh $@
